//Puerto
process.env.PORT = process.env.PORT || 3000

//Entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

//Vencimiento de Token
//60 segundos * 60 minutos *24 horas *30 dias
process.env.CADUCIDAD_TOKEN = 60*60*24*30

//Seed de autentificacion
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo'

//GoogleSign In client id global variable
process.env.CLIENT_ID = process.env.CLIENT_ID || '156881609144-jb5uqvvsqfn9ta94ui4jgghpd2e6iah7.apps.googleusercontent.com'


let urlDB

/*
if(process.env.NODE_ENV === 'dev'){
    urlDB = 'mongodb:localhost:27017/lab08'
}else{
    urlDB = 'mongodb+srv://jluis:4U9HyWo6fkEorAvk@cluster0.o4nix.mongodb.net/lab08'
}
*/

urlDB = 'mongodb+srv://jluis:4U9HyWo6fkEorAvk@cluster0.o4nix.mongodb.net/lab08'

process.env.URLDB = urlDB