const jwt = require("jsonwebtoken");
//yarn add node-localstorage  - Dependency needed for localstorage
//https://www.quora.com/How-can-I-access-the-value-on-localStorage-from-Node-js
if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
}

//Verifica token
let verificaToken = (req,res,next) => {

    //let token = req.get("token");
    var token = localStorage.getItem('myToken');
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        
        console.log(process.env.SEED);
        if (err){
            return res.redirect("/");
            /*
            return res.status(400).json({
                ok:false,
                err,
            });
            */
        }
        req.usuario  = decoded.usuario;
        console.log(req.usuario);
        console.log("--------------");
        console.log(decoded.usuario);
        //console.log(decoded);
        next();
    });

};

module.exports = {verificaToken,};

