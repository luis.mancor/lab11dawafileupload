const express = require("express");
const Pet = require("../models/pets");  //persistency use for model PET
const {verificaToken} = require("../middlewares/authentication"); //function for token validation
const app = express();
const methodOverride = require("method-override"); //dependency for put delete request instalation via npm install method-override

const fs = require('fs');
const path = require('path');

app.use(methodOverride('_method'));  //USE OF METHODOVERRIDE

//Use of localstorage 
if (typeof localStorage === "undefined" || localStorage === null) {  //Dependency that allows use of localstorage similar to cookie and session handling
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
}


//-----------------------PET REGISTRATION--------------------------//
app.get("/petRegistration",verificaToken, function(req,res){
    res.render('petRegister')
})

app.post("/pet",verificaToken, function (req, res){  
    let body = req.body;
    var userID = localStorage.getItem('usuarioID');
    var ID = localStorage.getItem('id');
    //console.log(body);
    //console.log(req.files);

    if(!req.files){
        return res.status(400).json({
            ok:false,
            err: {
                message: "No se ha seleccionado ningun archivo",
            },
        });
    }
    let archivo = req.files.archivo;
    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    let extensionesValidas = ["png", "jpg", "gif", "jpeg"];
    if (extensionesValidas.indexOf(extension) < 0){
        return res.status(400).json({
            ok: false,
            err:{
                message: "Las extensiones permitidas son "+ extensionesValidas.join(", "),
                ext:extension,
            },
        });
    }
    let nombreArchivo = `${ID}-${new Date().getMilliseconds()}.${extension}`;
    archivo.mv(`public/images/${nombreArchivo}`, (err) => {
        if (err){
            return res.status(500).json({
                ok:false,
                err,
            });
        }
    }); 

    let pet = new Pet({
        nombre: body.nombre,
        edadmascota:body.edadmascota,
        ubicacion: body.ubicacion,
        tamano: body.tamano,
        sexo:body.sexo,
        estado: body.estado,
        historia: body.historia,
        usuarioSubmmited: userID,
        img: nombreArchivo,
    });

    pet.save((err, petDB) => {
        if (err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.redirect('/pet');
        console.log(petDB);
    });
    
});

//-----------------------PET LISTINGS--------------------------//
app.get("/pet",verificaToken, function (req, res){
    Pet.find({}).exec((err, pets) => {
        if (err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.render('petList',{
            "data":pets,
        });
        console.log(pets);

    });
});

//-----------------------PET EDITING--------------------------//
app.get("/petEdit/:id",verificaToken ,async function (req,res){
    let id = req.params.id;
    const pet =  await Pet.findById(req.params.id);
    console.log(pet);
    console.log(pet.historia);
    res.render('petEdit',{
        pet, 
    });
});

app.post("/pet/:id",verificaToken, function (req, res) {
    let id = req.params.id;
    let body = req.body;
    var ID = localStorage.getItem('id');

    let nombre = body.nombre;
    let edad = body.edadmascota;
    let ubicacion = body.ubicacion;
    let tamano = body.tamano;
    let sexo =body.sexo;
    let historia = body.historia;
    let usuarioSubmmited = localStorage.getItem('usuarioID');

    //--section in work
    if(!req.files){
        return res.status(400).json({
            ok:false,
            err: {
                message: "No se ha seleccionado ningun archivo",
            },
        });
    }
    let archivo = req.files.archivo;
    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    let extensionesValidas = ["png", "jpg", "gif", "jpeg"];
    if (extensionesValidas.indexOf(extension) < 0){
        return res.status(400).json({
            ok: false,
            err:{
                message: "Las extensiones permitidas son "+ extensionesValidas.join(", "),
                ext:extension,
            },
        });
    }
    let nombreArchivo = `${ID}-${new Date().getMilliseconds()}.${extension}`;

    archivo.mv(`public/images/${nombreArchivo}`, (err) => {
        if (err){
            return res.status(500).json({
                ok:false,
                err,
            });
        }
    }); 

    Pet.findById(id, (err, petDB) => {
        if (err){
            return res.status(500).json({
                ok:false,
                err,
            });
        }
        if(!petDB){
            return res.status(400).json({
                ok:false,
                err: {
                    message: "Usuario no existe",
                }
            });
        }
        let pathImagen = path.resolve(__dirname, `../public/images/${petDB.img}`);
        if(fs.existsSync(pathImagen)){
            fs.unlinkSync(pathImagen);
            //console.log("---Imagen borrada");
        }
        //console.log("nuevo path: "+pathImagen);
        petDB.img = nombreArchivo;
        petDB.nombre = nombre;
        petDB.edad = edad;
        petDB.ubicacion = ubicacion;
        petDB.tamano = tamano;
        petDB.sexo = sexo;
        petDB.historia = historia;
        petDB.usuarioSubmmited = usuarioSubmmited;

        petDB.save((err, usuarioGuardado) =>{
            //Salida
            /*
            res.json({
                ok: true,
                usuario: usuarioGuardado,
                img:nombreArchivo,

            });
            */
            res.redirect("/pet");
        });
    });
    //section in work end

    /* //PREVIOUS UPDATE RAW DATA
    Pet.findByIdAndUpdate(id, body, {new: true}, (err, petDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.redirect("/pet");
    });
    */
});


//-----------------------PET DELETION--------------------------//
app.delete("/pet/:id",verificaToken ,function(req, res){

    let id = req.params.id;
    Pet.findByIdAndDelete(id, (err, petBorrada)=>{
        if (err){
            return res.status(400).json({
                ok:false,
                err,
            });
        }
        console.log(petBorrada);
        res.redirect('/pet');
        /*
        res.json({
            ok:true,
            pet: petBorrada,
        });
        */
    });
});


//Function for returning images

app.get("/imagen/:img", (req,res)=>{
    let img = req.params.img;

    let pathImagen = path.resolve(__dirname, `../public/images/${img}`);
    if(fs.existsSync(pathImagen)){
        res.sendFile(pathImagen);
        //http://localhost:3000/imagen/${pet.img}

    }else{
        let noImagePath = path.resolve(__dirname, `../public/images/notfound.png`);
        res.sendFile(noImagePath);
    }
});



module.exports = app;