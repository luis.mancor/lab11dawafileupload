const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Usuario = require("../models/usuario");
const {verificaToken} = require("../middlewares/authentication"); //function for token validation

//Google sign in imports
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(process.env.CLIENT_ID);


if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
}

const app = express();

//Async verify token
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();

    return payload;
}
//Google sign in token
app.post("/google", async (req,res) => {
    let token = req.body.idtoken;
    //console.log("ESTA ES LA ID .."+token);

    let googleUser = await verify(token).catch((e) => {
        return res.status(403).json({
            ok: false,
            err: e,
        });       
    });
    
    console.log(googleUser);

    Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) =>{
        if (err){
            return res.status(500).json({
                ok:false,
                err,
            });
        }
        if(usuarioDB) {
            if (usuarioDB.google === false){
                return res.status(400).json({
                    ok:false,
                    err: {
                        message: "Debe usar autentificacion normal",
                    },
                });
            }else{
                let token = jwt.sign(
                    {usuario: usuarioDB,},
                    process.env.SEED, 
                    { expiresIn: process.env.CADUCIDAD_TOKEN }
                    );
                //Localstorage to realize actions within the app
                localStorage.setItem('myToken', token);
                localStorage.setItem('usuarioID', usuarioDB.email);
                localStorage.setItem('nombre', usuarioDB.nombre);
                localStorage.setItem('id', usuarioDB._id);
                return res.redirect("/");
                /*    
                return res.json({
                    ok: true,
                    usuario:usuarioDB,
                    token,
                });
                */
            }
        } else {
            //El user no existe en la DB
            let usuario = new Usuario();

            usuario.nombre = googleUser.name;
            usuario.email = googleUser.email;
            usuario.img = googleUser.img;
            usuario.google = true;
            usuario.password = "123";

            usuario.save((err,usuarioDB) => {
                if(err){
                    return res.status(500).json({
                        ok:false,
                        err,
                    });
                }
                let token = jwt.sign({usuario: usuarioDB,}, process.env.SEED, {expiresIn: process.env.CADUCIDAD_TOKEN});
                
                return res.json({
                    ok: true,
                    usuario: usuarioDB,
                    token,
                });
            });
        }


    });
    
});




app.get("/", function(req,res){
    try{
        var token = localStorage.getItem('myToken');
        var nombre = localStorage.getItem('nombre');
    }catch(err){
        console.log('No existe token');
    }
    res.render('index',{
        data:token,
        nombre:nombre,
    })
});

app.get("/login", function(req,res){
    res.render('login')
});

app.post("/login", (req, res) => {
    let body = req.body;

    Usuario.findOne({email: body.email}, (err, usuarioDB) => {
        //Control de error
        if (err){
            return res.status(500).json({
                ok:false,
                err,
            });
        }
        //Control de usuario en blanco
        if(!usuarioDB){
            return res.status(400).json({
                ok:false,
                err:{
                    message: "(Usuario) o contraseña incorrectas",
                },
            });
        }
        //Control de que las contraseñas no son iguales
        if(!bcrypt.compareSync(body.password, usuarioDB.password)){
            return res.status(400).json({
                ok: false,
                err:{
                    message: "Usuario o (contraseña) incorrectos",
                },
            });
        }
        //Generacion de token y paso al json
        let token = jwt.sign({usuario: usuarioDB}, process.env.SEED, {expiresIn: process.env.CADUCIDAD_TOKEN})

        localStorage.setItem('myToken', token);
        localStorage.setItem('usuarioID', usuarioDB.email);
        localStorage.setItem('nombre', usuarioDB.nombre);
        localStorage.setItem('id', usuarioDB._id);
        /*
        res.json({
            ok: true,
            usuario: usuarioDB,
            token,
        });
        */
        res.render('welcome',{
            data: usuarioDB,
            nombre:usuarioDB.nombre,
        });
        console.log(usuarioDB);
    });
});

app.get("/welcome", verificaToken ,function(req,res){
    try{
        var token = localStorage.getItem('myToken');
        var nombre = localStorage.getItem('nombre');
    }catch(err){
        console.log('No existe token');
    }
    res.render('welcome',{
        data:token,
        nombre:nombre,
    })
});

app.get("/logout", function(req,res){


    localStorage.removeItem('myToken');
    localStorage.removeItem('usuarioID');
    localStorage.removeItem('nombre');
    localStorage.removeItem('id');
    res.redirect('/login');

    
});

app.post("/logout", function (req, res){
    let auth2 = req.body.auth2;
    /*
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut()

    gapi.load('auth2', function() {
        gapi.auth2.init();
    });
    */
    localStorage.removeItem('myToken');
    localStorage.removeItem('usuarioID');
    localStorage.removeItem('nombre');
    localStorage.removeItem('id');
    res.redirect('/login/');

});

module.exports = app;

/*
function checkLogin(req,res,next){
    var myToken = localStorage.getItem('myToken');
    try{
        jwt.verify(myToken, process.env.SEED);
    }catch(err){
        return res.status(500).json({
            ok:false,
            err,
        });
    }
    next();
}
*/